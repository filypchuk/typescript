import { createElement } from './helpers/domHelper'; 
import { NewElement } from './models/new-element';
import { IElementAttributes } from './models/element-attributes';
import { IFighter } from './models/fighter';

export function createFighter(fighter:IFighter, handleClick:any, selectFighter:any) {

  const nameElement = createName(fighter.name);
  const imageElement = createImage(fighter.source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement(new NewElement('div','fighter'));
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev:MouseEvent) =>  ev.stopPropagation();
  const onCheckboxClick = (ev:Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev:MouseEvent) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name:string) {
  const nameElement = createElement(new NewElement('span','name'));
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source:string) {
  const attributes: IElementAttributes =  { src: source };
  const imgElement = createElement(new NewElement('img','fighter-image', attributes));

  return imgElement;
}

function createCheckbox() {
  const label = createElement(new NewElement('label','custom-checkbox'));
  const span = createElement(new NewElement('span','checkmark'));
  const attributes: IElementAttributes = { type: 'checkbox' };
  const checkboxElement = createElement(new NewElement('input',"", attributes));

  label.append(checkboxElement, span);
  return label;
}