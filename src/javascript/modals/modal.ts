import { createElement } from '../helpers/domHelper';
import { NewElement } from '../models/new-element';
import { IModal } from '../models/modal';

export function showModal(modalElement:IModal) {
  const root: HTMLElement = getModalContainer();
  const modal = createModal(modalElement);
  
  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root') as HTMLElement;
}

function createModal(modal:IModal) {
  const layer = createElement(new NewElement('div','modal-layer'));
  const modalContainer = createElement(new NewElement('div', 'modal-root' ));
  const header = createHeader(modal.title);

  modalContainer.append(header, modal.bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title:string) {
  const headerElement = createElement(new NewElement('div','modal-header'));
  const titleElement = createElement(new NewElement('span'));
  const closeButton = createElement(new NewElement('div','close-btn'));
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal(event:MouseEvent) {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
