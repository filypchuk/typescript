import { createElement } from '../helpers/domHelper';
import { NewElement } from '../models/new-element';
export function showModal(modalElement) {
    const root = getModalContainer();
    const modal = createModal(modalElement);
    root.append(modal);
}
function getModalContainer() {
    return document.getElementById('root');
}
function createModal(modal) {
    const layer = createElement(new NewElement('div', 'modal-layer'));
    const modalContainer = createElement(new NewElement('div', 'modal-root'));
    const header = createHeader(modal.title);
    modalContainer.append(header, modal.bodyElement);
    layer.append(modalContainer);
    return layer;
}
function createHeader(title) {
    const headerElement = createElement(new NewElement('div', 'modal-header'));
    const titleElement = createElement(new NewElement('span'));
    const closeButton = createElement(new NewElement('div', 'close-btn'));
    titleElement.innerText = title;
    closeButton.innerText = '×';
    closeButton.addEventListener('click', hideModal);
    headerElement.append(title, closeButton);
    return headerElement;
}
function hideModal(event) {
    const modal = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
}
