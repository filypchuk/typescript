import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { NewElement } from "../models/new-element";
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title: title, bodyElement: bodyElement });
}
function createFighterDetails(fighter) {
    const fighterDetails = createElement(new NewElement('div', 'modal-body'));
    const nameElement = createElement(new NewElement('p', 'fighter-name'));
    const attackElement = createElement(new NewElement('p', 'fighter-attack'));
    const defenseElement = createElement(new NewElement('p', 'fighter-defense'));
    const healthElement = createElement(new NewElement('p', 'fighter-health'));
    const imgAtteibute = { 'src': fighter.source };
    const imageElement = createElement(new NewElement('img', 'fighter-image', imgAtteibute));
    nameElement.innerText = fighter.name;
    attackElement.innerText = `attack - ${fighter.attack}`;
    defenseElement.innerText = `defense - ${fighter.defense}`;
    healthElement.innerText = `health - ${fighter.health}`;
    fighterDetails.append(imageElement, nameElement, attackElement, attackElement, defenseElement, healthElement);
    return fighterDetails;
}
