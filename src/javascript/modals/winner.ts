import { IFighterDetail } from "../models/fighter-details";
import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";
import { NewElement } from "../models/new-element";
import { IElementAttributes } from "../models/element-attributes";
import { IModal } from "../models/modal";

export  function showWinnerModal(fighter: IFighterDetail) {
  const title: string = 'Winner!';
  const bodyElement: HTMLElement = createWiner(fighter);
  showModal({title:title, bodyElement:bodyElement } as IModal); 
}

function createWiner(fighter: IFighterDetail) {

  const fighterDetails: HTMLElement = createElement(new NewElement('div', 'modal-body'));
  const nameElement: HTMLElement = createElement(new NewElement('p','name'));
  nameElement.innerText = fighter.name;  
  const imgAtteibute:IElementAttributes = {'src':fighter.source }
  const imageElement: HTMLElement = createElement(new NewElement('img','fighter-image', imgAtteibute));


  fighterDetails.append(imageElement, nameElement);

  return fighterDetails;
}

