import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { NewElement } from "../models/new-element";
import { IElementAttributes } from '../models/element-attributes';
import { IFighterDetail } from '../models/fighter-details';
import { IModal } from '../models/modal';

export  function showFighterDetailsModal(fighter: IFighterDetail) {
  const title:string = 'Fighter info';
  const bodyElement: HTMLElement = createFighterDetails(fighter);
  showModal({title:title, bodyElement:bodyElement} as IModal);
}

function createFighterDetails(fighter: IFighterDetail) {

  const fighterDetails: HTMLElement = createElement(new NewElement('div', 'modal-body'));
  const nameElement: HTMLElement = createElement(new NewElement('p','fighter-name'));
  const attackElement: HTMLElement = createElement(new NewElement('p', 'fighter-attack'));
  const defenseElement: HTMLElement = createElement(new NewElement('p','fighter-defense'));
  const healthElement: HTMLElement = createElement(new NewElement('p','fighter-health'));

  const imgAtteibute:IElementAttributes = {'src':fighter.source }
  const imageElement: HTMLElement = createElement(new NewElement('img','fighter-image', imgAtteibute));

  nameElement.innerText = fighter.name;
  attackElement.innerText = `attack - ${fighter.attack}`;
  defenseElement.innerText = `defense - ${fighter.defense}`;
  healthElement.innerText = `health - ${fighter.health}`;


  fighterDetails.append(imageElement, nameElement, attackElement, attackElement, defenseElement, healthElement);

  return fighterDetails;
}
