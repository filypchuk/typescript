import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";
import { NewElement } from "../models/new-element";
export function showWinnerModal(fighter) {
    const title = 'Winner!';
    const bodyElement = createWiner(fighter);
    showModal({ title: title, bodyElement: bodyElement });
}
function createWiner(fighter) {
    const fighterDetails = createElement(new NewElement('div', 'modal-body'));
    const nameElement = createElement(new NewElement('p', 'name'));
    nameElement.innerText = fighter.name;
    const imgAtteibute = { 'src': fighter.source };
    const imageElement = createElement(new NewElement('img', 'fighter-image', imgAtteibute));
    fighterDetails.append(imageElement, nameElement);
    return fighterDetails;
}
