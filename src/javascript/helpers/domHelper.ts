import { NewElement } from "../models/new-element";

export function createElement(newElement:NewElement) {
  const element = document.createElement(newElement.tagName);
  
  if (newElement.className) {
    element.classList.add(newElement.className);
  }

  Object.keys(newElement.attributes).forEach(key => element.setAttribute(key, newElement.attributes[key]));

  return element;
}

