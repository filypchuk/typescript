import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { IFighter } from './models/fighter';

const rootElement: HTMLElement = document.getElementById('root') as HTMLElement;
const loadingElement: HTMLElement = document.getElementById('loading-overlay') as HTMLElement;

export async function startApp() {
  try {
    loadingElement.style.visibility = 'visible';
    
    const fighters: IFighter[] = await getFighters();
    const fightersElement = createFighters(fighters);

    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}
