export function fight(firstFighter, secondFighter) {
    let healthFirstFighter = firstFighter.health;
    let healthSecondFighter = secondFighter.health;
    while (true) {
        healthFirstFighter -= getDamage(secondFighter, firstFighter);
        if (healthFirstFighter <= 0) {
            return secondFighter;
        }
        healthSecondFighter -= getDamage(firstFighter, secondFighter);
        if (healthSecondFighter <= 0) {
            return firstFighter;
        }
    }
}
export function getDamage(attacker, enemy) {
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    if (damage <= 0) {
        damage = 0;
    }
    return damage;
}
export function getHitPower(fighter) {
    let criticalHitChance = Math.random();
    let hitPower = fighter.attack * criticalHitChance;
    return hitPower;
}
export function getBlockPower(fighter) {
    let dodgeChance = Math.random();
    let blockPower = fighter.defense * dodgeChance;
    return blockPower;
}
