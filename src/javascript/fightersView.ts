import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter } from './models/fighter';
import { getFighterDetails } from './services/fightersService';
import { NewElement } from './models/new-element';
import { IFighterDetail } from './models/fighter-details';

export function createFighters(fighters: IFighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement(new NewElement('div','fighters'));

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

async function showFighterDetails(event: Event, fighter: IFighterDetail) {
  const fullInfo: IFighterDetail  = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<IFighterDetail> {
  const fighter: IFighterDetail = await getFighterDetails(fighterId);
  return fighter;
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterDetail>();

  return async function selectFighterForBattle(event:any, fighter: IFighterDetail) {
    
    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const twoFighters = Array.from(selectedFighters.values())
      const winner = fight(twoFighters[0], twoFighters[1]);
      showWinnerModal(winner);
    }
  }
}
