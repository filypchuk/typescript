import { callApi } from '../helpers/apiHelper';
import { IFighter } from '../models/fighter';
import { IFighterDetail } from '../models/fighter-details';

export async function getFighters() : Promise<IFighter[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET') as IFighter[];
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id:string) : Promise<IFighterDetail> {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET') as IFighterDetail;
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

