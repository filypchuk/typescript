export class NewElement {
    constructor(tagName, className = "", attributes = {}) {
        this.tagName = tagName;
        this.attributes = attributes;
        this.className = className;
    }
}
