export interface IModal{
    title: string;
    bodyElement: HTMLElement;
  }