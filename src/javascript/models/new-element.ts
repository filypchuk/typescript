import { IElementAttributes } from "./element-attributes";

export class NewElement {
    tagName:string;
    className:string;
    attributes:IElementAttributes;
    
    constructor(tagName:string, className:string = "", attributes:IElementAttributes = {}) {
      this.tagName = tagName;
      this.attributes = attributes;
      this.className = className;
    } 
  }