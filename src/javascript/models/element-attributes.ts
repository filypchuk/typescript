export interface IElementAttributes {
    [key: string]:string;
  }