import { IFighterDetail } from "./models/fighter-details";

export function fight(firstFighter: IFighterDetail, secondFighter:IFighterDetail):IFighterDetail {

  let healthFirstFighter: number = firstFighter.health;
  let healthSecondFighter: number = secondFighter.health;

  while(true) {

    healthFirstFighter -= getDamage(secondFighter, firstFighter);   
    if(healthFirstFighter <= 0){
      return secondFighter;
    }

    healthSecondFighter -= getDamage(firstFighter, secondFighter);
    if(healthSecondFighter <= 0){
      return firstFighter;
    }
  }  
}

export function getDamage(attacker: any, enemy: any) {

  let damage: number = getHitPower(attacker) - getBlockPower(enemy);

  if(damage <= 0) {
    damage = 0;
  }
  return damage;
}

export function getHitPower(fighter:IFighterDetail): number {
  let criticalHitChance:number = Math.random();
  let hitPower: number = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter:IFighterDetail): number {
  let dodgeChance:number = Math.random();
  let blockPower:number = fighter.defense * dodgeChance;
  return blockPower;
}
